<?php
$title = "Add person";
$add_button = false;
include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/Controller/" . "PersonCtrl.php";
include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/Model/" . "Person.php";
include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/View/" . "header.php";
include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/View/" . "footer.php";


$newPerson = new Person("", "");
$person_controller = new PersonCtrl();

if(array_key_exists('addBtn', $_POST)) {
    addPerson();
}

function addPerson(){
    global $newPerson, $person_controller;
    $newPerson->setName(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING));
    $newPerson->setLastName(filter_input(INPUT_POST, 'last_name', FILTER_SANITIZE_STRING));
    $person_controller->addOne($newPerson);
    header("Location:/SecondProjectPhp");
}


?>

<form class="m-2 mb-5" action="" method="post">
    <div class="form-group">
        <label for="exampleInputEmail1">Name</label>
        <input type="text" class="form-control" name="name" autocomplete="off" aria-describedby="emailHelp" placeholder="Enter name">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Last name</label>
        <input type="text" class="form-control" name="last_name" autocomplete="off" placeholder="Enter last name">
    </div>
    <button type="submit" name="addBtn" class="btn btn-primary">Add</button>
</form>
<a type="button" href="<?php echo '/SecondProjectPhp/' ?>" class="btn btn-danger m-2 mb-5">Back</a>