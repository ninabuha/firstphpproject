<form class="form-inline mb-5 m-2" action="" method="post">
    <input class="form-control mr-sm-2" type="search" name="searchText" autocomplete="off" placeholder="Search" aria-label="Search person">
    <button class="btn btn-outline-success my-2 my-sm-0" name="searchBtn" type="submit">Search</button>
</form>
