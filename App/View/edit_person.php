<?php
$title = "Edit person";
$add_button = false;
include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/Controller/" . "PersonCtrl.php";
include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/Model/" . "Person.php";
include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/View/" . "header.php";
include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/View/" . "footer.php";



$person_controller = new PersonCtrl();
$person = new Person("", "");
$p = $person_controller->getOne(intval($_GET['id']));
$person->setId($p->id);
$person->setName($p->name);
$person->setLastName($p->last_name);

function editPerson(){
    global $person;
    $person->setName(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING));
    $person->setLastName(filter_input(INPUT_POST, 'last_name', FILTER_SANITIZE_STRING));
    global $person_controller;
    $person_controller->editOne($person);
}

function deletePerson(){
    global $person;
    global $person_controller;
    $person_controller->deleteOne($person);
    header("location:/SecondProjectPhp");
}

if(array_key_exists('changeBtn', $_POST)) {
    editPerson();
}
elseif(array_key_exists('deleteBtn', $_POST)) {
    deletePerson();
}
?>

<form class="m-2 mb-5" action="" method="post">
    <div class="form-group"
    <label for="name">Name</label>
    <input type="text" class="form-control" autocomplete="off" name="name" value="<?php echo $person->getName() ?>">
    </div>
    <div class="form-group">
        <label for="last_name">Last name</label>
        <input type="text" class="form-control" autocomplete="off" name="last_name" value="<?php echo $person->getLastName() ?>">
    </div>
    <button type="submit" name="changeBtn" class="btn btn-primary">Change</button>
    <button type="submit" name="deleteBtn" class="btn btn-danger">Delete</button>
</form>
<a type="button" name="changeBtn" href="<?php echo '/SecondProjectPhp/' ?>" class="btn btn-danger m-2 mb-5">Back</a>
