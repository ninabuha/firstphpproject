<?php
    include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/Model/" . "Person.php";
    include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/Controller/" . "PersonCtrl.php";
    include $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/View/" . "search.php";

    $person = new Person("", "");
    $person_controller = new PersonCtrl();



    if(array_key_exists('searchBtn', $_POST)) {
        search();
    }
    else{
        showAll();
    }


    function showAll(){
        global $person, $person_controller;
        $people = $person_controller->getAll();
        foreach ($people as $p){
            $person->setId($p->id);
            $person->setName($p->name);
            $person->setLastName($p->last_name);
            include "person_view.php";
        }
    }

    function search(){
        global $person_controller, $people;
        $people = array();
        $person = new Person("", "");
        if(filter_input(INPUT_POST, 'searchText', FILTER_SANITIZE_STRING)    == ""){
            showAll();
        }
        else{
            $people = $person_controller->search(filter_input(INPUT_POST, 'searchText', FILTER_SANITIZE_STRING));
        }

        foreach ($people as $p){
            $person->setId($p->id);
            $person->setName($p->name);
            $person->setLastName($p->last_name);
            include "person_view.php";
        }
    }