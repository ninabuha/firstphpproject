<form class="m-2 mb-5" action="App/View/edit_person.php?id=<?php echo $person->getId() ?>" method="post">
    <div class="form-group"
        <label for="exampleInputEmail1">Name</label>
        <input type="text" class="form-control" name="name" aria-describedby="emailHelp" value="<?php echo $person->getName() ?>">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Last name</label>
        <input type="text" class="form-control" name="last_name" value="<?php echo $person->getLastName() ?>">
    </div>
    <button type="submit" class="btn btn-primary">View</button>
</form>