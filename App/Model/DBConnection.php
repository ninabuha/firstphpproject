<?php


class DBConnection
{
    private $connection;

    public function __construct()
    {
        $this->connection = new PDO("mysql:host=localhost; dbname=person_db_php; charset=utf8", "root", "root");
        $this->connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param mixed $connection
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
    }



}