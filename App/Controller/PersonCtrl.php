<?php

require $_SERVER['DOCUMENT_ROOT'] . "/SecondProjectPhp/App/Model/" . "DBConnection.php";

class PersonCtrl
{
    private $people;
    private $person;
    private $dbConnection;

    public function __construct()
    {
        $this->dbConnection = new DBConnection();
    }

    public function getAll(){
        $prep_st = "SELECT * FROM person;";
        $prep = $this->dbConnection->getConnection()->prepare($prep_st);
        if(!$prep){
            die("SQL statement not correct");
        }
        $prep->execute();
        $this->people = $prep->fetchAll(PDO::FETCH_OBJ);
        return $this->people;
    }

    public function getOne($id){
        $prep_st = "SELECT * FROM person WHERE id=?;";
        $prep = $this->dbConnection->getConnection()->prepare($prep_st);
        $prep->execute([$id]);
        $this->person = $prep->fetch(PDO::FETCH_OBJ);
        return $this->person;
    }

    public function editOne($person){
        $prep_st = "UPDATE person SET name=?, last_name=? WHERE id=?";
        $prep = $this->dbConnection->getConnection()->prepare($prep_st);
        $prep->execute([$person->getName(), $person->getLastName(), $person->getId()]);
        $this->people = $prep->fetchAll(PDO::FETCH_OBJ);
    }

    public function deleteOne($person){
        $prep_st = "DELETE FROM person WHERE id=?";
        $prep = $this->dbConnection->getConnection()->prepare($prep_st);
        $prep->execute([$person->getId()]);
        $this->people = $prep->fetchAll(PDO::FETCH_OBJ);
    }

    public function addOne($person){
        $prep_st = "INSERT INTO person (name, last_name) VALUES(?, ?)";
        $prep = $this->dbConnection->getConnection()->prepare($prep_st);
        $prep->execute([$person->getName(), $person->getLastName()]);
        $this->people = $prep->fetchAll(PDO::FETCH_OBJ);
    }

    public function search($searchText){
        $prep_st = "SELECT * from person WHERE CONCAT(name, ' ', last_name) LIKE ?";
        $prep = $this->dbConnection->getConnection()->prepare($prep_st);
        $prep->execute(["%".$searchText."%"]);
        $this->people = $prep->fetchAll(PDO::FETCH_OBJ);
        return $this->people;
    }

    /**
     * @return mixed
     */
    public function getPeople()
    {
        return $this->people;
    }

    /**
     * @param mixed $people
     */
    public function setPeople($people)
    {
        $this->people = $people;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param mixed $person
     */
    public function setPerson($person)
    {
        $this->person = $person;
    }

    /**
     * @return DBConnection
     */
    public function getDbConnection()
    {
        return $this->dbConnection;
    }

    /**
     * @param DBConnection $dbConnection
     */
    public function setDbConnection($dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }



}